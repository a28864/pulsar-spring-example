package com.example.pulsarexample;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.pulsar.core.PulsarTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

@Data
@Slf4j
@Component
public class MyPublisher {
    private final PulsarTemplate<FooEntity> template;

    @Scheduled(cron = "0/30 * * * * *")
    public void publishJob() throws ExecutionException, InterruptedException {
        var entities = IntStream.range(0, 10000000).mapToObj(i -> new FooEntity(UUID.randomUUID().toString())).toList();

        var futures = entities.stream().parallel().map(entity -> {
            try {
                return template.sendAsync("my-topic", entity);
            } catch (PulsarClientException e) {
                throw new RuntimeException("Error publishing message " + entity.getId(), e);
            }
        }).toList();

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).get();
    }
}
