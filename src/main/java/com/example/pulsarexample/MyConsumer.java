package com.example.pulsarexample;

import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.common.schema.SchemaType;
import org.apache.pulsar.reactive.client.api.MessageResult;
import org.springframework.pulsar.reactive.config.annotation.ReactivePulsarListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class MyConsumer {
    @ReactivePulsarListener(
        topics = "my-topic",
        stream = true,
        schemaType = SchemaType.JSON,
        concurrency = "3",
        subscriptionName = "my-consumer"
    )
    Flux<MessageResult<Void>> listen2(Flux<Message<FooEntity>> messages) {
        return messages.doOnNext((msg) -> {
            // noop
        }).map(MessageResult::acknowledge);
    }
}
