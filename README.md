# Pulsar/Spring Playground

This is a very simple app that utilizes Spring Boot and its Pulsar integration to demonstrate how
applications may be written to interact with Pulsar in a simplistic way.

## Running Pulsar

The `docker` directory contains a docker compose manifest.

Before starting the application, 2 directories must be created: one for Pulsar configuration,
and one for Pulsar data. These directories are used as bind mount volumes for the container so that
Pulsar's configuration and actual data are exported outside the container, and thus persist through
instantiations of the container. From the `docker` directory, run:

```shell
mkdir data conf
```

To run the container, you must have docker installed and running. To start the container, run the
following from the `docker` directory:

```shell
docker compose up -d
```

This will start the container in the background. If you would like to observe the logs, simply omit
the `-d` option.

Likewise, to stop the container, run:

```shell
docker compose down
```

## Running the Application

The application is a typical Spring Boot application written for JDK 17 that utilizes Gradle as its
build tooling. It is configured to talk to the Pulsar instance as configured in the docker compose
manifest, and should require no additional configuration. The Pulsar instance must be alive to start
the application.

To start the app, run the following from the root of the project:

```shell
./gradlew bootRun
```

## About the Application

The application configures a Pulsar topic (named `my-topic`) with 5 partitions. Every 30 seconds, the
`myPublisher` bean publishes 4 million messages to this topic.

Messages are of a complex type but are themselves simple. The `FooEntity` class implements the shape of
the message and simply contains a single string identifier as a property. The publisher uses a randomly
generated UUID for this ID.

Messages are streamed and consumed in the `myConsumer` bean. This is accomplished through the use of
reactive streams (utilizing Project Reactor). Messages are consumed and acknowledged, however, there
is no logic applied to messages.

On some regular cadence, the underlying Pulsar libraries will output statistics that include throughput.
